import usaddress
import json
import re
from collections import OrderedDict


def addressParser(inputAddress):

    parsedAddress = usaddress.parse(inputAddress)
    try:
        try:
            parsedHouseNumber = [element[0] for element in parsedAddress
                                 if element[1] in ['AddressNumber','OccupancyIdentifier','USPSBoxID',"AddressNumberSuffix"]]
            HouseNumber=' '.join([str(x) for x in parsedHouseNumber])
        except IndexError:
            HouseNumber = ''

        street = re.sub(
                        HouseNumber, '',
                        inputAddress,
                        flags=re.IGNORECASE
                        ).rstrip(
                                    '}{[]()?@$%^*<>/\\\"\'~;:-_, '
                        ).lstrip(
                                    '}{[]()?@$%^*<>/\\\"\'~;:-_,. '
                        ).replace(
                                    '  ', ' '
                        ).replace(
                                    ' ,', ','
                        )
        HouseNumber = re.sub(
                        street, '',
                        inputAddress,
                        flags=re.IGNORECASE
                        ).rstrip(
                                    '}{[]()?@$%^*<>/\\\"\'~;:_, '
                        ).lstrip(
                                    '}{[]()?@$%^*<>/\\\"\'~;:_,. '
                        ).replace(
                                    '  ', ' '
                        ).replace(
                                    ' ,', ','
                        )

        addressDict = OrderedDict([("street", street),
                                  ("housenumber", HouseNumber)])
        # return json using orderedDict
        return json.dumps(addressDict, ensure_ascii=False)
    except Exception as e:
        print(e)


def main():
    print("Enter \"quit\" to exit the script.")

    while(1):
        inputAddress = input("\nEnter address: \n")

        if(inputAddress == "quit"):
            break
        else:
            print(addressParser(inputAddress))


if __name__ == "__main__":
    main()