# Parse address

Python script to parse address with concatenated street names and numbers.


## Examples of parsing

**Input:** string of address

**Output:** string of street and string of street-number as JSON object

1. `"Winterallee 3"` -> `{"street": "Winterallee", "housenumber": "3"}`
2. `"Musterstrasse 45"` -> `{"street": "Musterstrasse", "housenumber": "45"}`
3. `"Blaufeldweg 123B"` -> `{"street": "Blaufeldweg", "housenumber": "123B"}`

## Environment

1. Python 3
2. [usaddress](https://pypi.org/project/usaddress/) : usaddress is a python library for parsing unstructured address strings into
address components, using advanced NLP methods.



## Getting Started and Running

### Run source code locally

1. Install dependencies

   ```
   pip install -r requirements.txt
   ```

2. Run Python script 

   ```
   python3 parseAddress.py
   ```

   

## Tests

Run the testing script. The testing script imports the testing dataset from`testCase.csv`.

```
python3 testParser.py 
```
