import parseAddress
import csv


def importTestCase(file_name):
    rows = []
    with open(file_name, encoding='utf-8-sig') as f:
        readers = csv.DictReader(f, delimiter=',')
        for row in readers:
            rows.append([row['Input'], row['Expected']])
    print("Imported DATA: ", file_name, "\n")
    return rows

def test_case():
    rows = importTestCase('testCase.csv')
    passed=0
    failed=0
    for row in rows:
        result = parseAddress.addressParser(row[0])    
        try:
            assert result == row[1]
            passed+=1
        except Exception:
            failed+=1
    print(f"Total Test case: {passed+failed}\nTest case passed {passed}\nTest case failed: {failed}")

if __name__ == '__main__':
    test_case()